from django.test import TestCase
from employee.tests.employee_factory import EmployeeFactory
from unittest import mock
from django.core.management import call_command

class EmployeeTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.employee = EmployeeFactory.create()


    def setUp(selfself):
        call_command("loaddata", "suite_test/dump.json", verbosity=0)

    def test_filds_label(self):

        name_label = self.employee._meta.get_field('name').verbose_name
        email_label = self.employee._meta.get_field('email').verbose_name
        department_label = self.employee._meta.get_field('department').verbose_name
        self.assertEquals(name_label, 'name')
        self.assertEquals(email_label, 'email')
        self.assertEquals(department_label, 'department')

    def test_field_max_length(self):
        name_max_length = self.employee._meta.get_field('name').max_length
        email_max_length = self.employee._meta.get_field('email').max_length
        department_max_length = self.employee._meta.get_field('department').max_length
        self.assertEquals(name_max_length, 255)
        self.assertEquals(email_max_length, 255)
        self.assertEquals(department_max_length, 150)

    def test_field_nullable(self):
        name_nullable = self.employee._meta.get_field('name').null
        email_nullable= self.employee._meta.get_field('email').null
        department_nullable= self.employee._meta.get_field('department').null
        self.assertEquals(name_nullable, False)
        self.assertEquals(email_nullable, False)
        self.assertEquals(department_nullable, False)

    def test_str(self):
        employee_str = "name: aurelio, email: aurelio@gmail.com, department: TI"

        self.assertEquals( self.employee.__str__(), employee_str)


    def test_cmp(self):
        copy_employee = self.employee
        self.assertTrue( copy_employee.__cmp__(self.employee), True)


