import unittest
from employee.tests.employee_factory import EmployeeFactory
from employee.models.serializer import EmployeeSerializer
from employee.models.employee import Employee
from django.core.management import call_command

class EmployeeSerializerTestCase(unittest.TestCase):

    def setUp(self):
        call_command("loaddata", "suite_test/dump.json", verbosity=0)
        self.employee_attributes = {
            'name': 'aurelio',
            'email': 'aureliodemoraes13@gmail.com',
            'department': 'TI'
        }

        self.serializer_data_invalid_name = {
            'name': '',
            'email': 'john@gmail.com',
            'department': 'RH'
        }

        self.serializer_data_invalid_email = {
            'name': 'john',
            'email': 'john',
            'department': 'RH'
        }

        self.serializer_data_invalid_department = {
            'name': 'john',
            'email': 'john@gmail.com',
            'department': ''
        }

        self.employee = Employee.objects.create(**self.employee_attributes)
        self.serializer = EmployeeSerializer(instance=self.employee)

    def test_contains_expected_fields(self):
        data = self.serializer.data

        self.assertEqual(set(data.keys()), set(['id', 'name', 'email', 'department']))

    def test_fields_content(self):
        data = self.serializer.data

        self.assertEqual(data['name'], self.employee_attributes['name'])
        self.assertEqual(data['email'], self.employee_attributes['email'])
        self.assertEqual(data['department'], self.employee_attributes['department'])

    def test_fiels_are_valid(self):
        serializer_invalid_name = EmployeeSerializer(data=self.serializer_data_invalid_name)
        serializer_invalid_email = EmployeeSerializer(data=self.serializer_data_invalid_email)
        serializer_invalid_department = EmployeeSerializer(data=self.serializer_data_invalid_department)

        serializer_valid_fields = EmployeeSerializer(data=self.employee_attributes)


        self.assertTrue(serializer_valid_fields.is_valid())

        self.assertFalse(serializer_invalid_name.is_valid())
        self.assertFalse(serializer_invalid_email.is_valid())
        self.assertFalse(serializer_invalid_department.is_valid())
