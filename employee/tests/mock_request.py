from employee.models.serializer import EmployeeSerializer

from employee.tests.employee_factory import EmployeeFactory
from employee.models.employee import Employee

def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data=None, status_code=200):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return {"json_data": self.json_data, "status_code": self.status_code}

    if args[0] == "http://localhost:8000/api/v1/employee/?page=1":
        serializer = EmployeeSerializer(instance=EmployeeFactory.create())
        return MockResponse(json_data={'results': serializer.data})
    else:
        return MockResponse(json_data={'detail': 'Invalid page.'}, status_code=404)


def mocked_requests_delete(*args, **kwargs):
    class MockResponse:

        def __init__(self, reason="No content.", status_code=204):
            self.reason = reason
            self.status_code = status_code

    if args[0] == "http://localhost:8000/api/v1/employee/1":
        return MockResponse()
    else:
        return MockResponse(  reason="Not found.", status_code=404 )


def mocked_requests_create(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data=None, status_code=200):
            self.json_data = json_data
            self.status_code = status_code

    if kwargs["data"]:
        employee = Employee.objects.create( **kwargs["data"] )

        serializer = EmployeeSerializer(instance=employee)
        return MockResponse( json_data={'results': serializer.data})
    else:
        return MockResponse(json_data={'detail': 'Internal server error.'}, status_code=500)


def mocked_requests_update(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data=None, status_code=200):
            self.json_data = json_data
            self.status_code = status_code


    if args[0] == "http://localhost:8000/api/v1/employee/1":
        employee = Employee.objects.create( **kwargs["data"] )

        serializer = EmployeeSerializer(instance=employee)
        return MockResponse( json_data={'results': serializer.data})
    else:
        return MockResponse(json_data={'detail': 'Internal server error.'}, status_code=500)