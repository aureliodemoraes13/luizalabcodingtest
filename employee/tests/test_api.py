from employee.api import EmployeeApi
from django.test import TestCase
from unittest import mock
from unittest.mock import MagicMock
import json
from employee.tests.mock_request import mocked_requests_get, mocked_requests_delete, mocked_requests_create, mocked_requests_update

import requests
import requests_mock
from rest_framework.test import APIRequestFactory
from django.test import Client
from django.core.management import call_command
from employee.models.serializer import EmployeeSerializer

from employee.tests.employee_factory import EmployeeFactory



class EmployeeApiTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.employeeApi = EmployeeApi()

    def setUp(selfself):
        call_command("loaddata", "suite_test/dump.json", verbosity=0)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_list_employee_success(self, mock_request):
        json_data = self.employeeApi.list(1)

        self.assertEqual( json_data["json_data"]["results"]["name"], "aurelio")
        self.assertEqual(json_data["json_data"]["results"]["email"], "aurelio@gmail.com")
        self.assertEqual(json_data["json_data"]["results"]["department"], "TI")
        self.assertEqual(json_data["status_code"], 200)
        self.assertIn(mock.call('http://localhost:8000/api/v1/employee/?page=1'), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_list_employee_fail(self, mock_request):
        json_data = self.employeeApi.list(3)
        self.assertEqual(json_data["json_data"]["detail"], "Invalid page.")
        self.assertEqual(json_data["status_code"], 404)

        self.assertIn(mock.call('http://localhost:8000/api/v1/employee/?page=3'), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)

    @mock.patch('requests.delete', side_effect=mocked_requests_delete)
    def test_delete_employee_success(self, mock_request):
        response = self.employeeApi.delete(1)
        self.assertEqual( response.reason, "No content.")
        self.assertEqual( response.status_code, 204)

        self.assertIn(mock.call('http://localhost:8000/api/v1/employee/1'), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)

    @mock.patch('requests.delete', side_effect=mocked_requests_delete)
    def test_delete_employee_fail(self, mock_request):
        response = self.employeeApi.delete(3)
        self.assertEqual( response.reason, "Not found.")
        self.assertEqual( response.status_code, 404)

        self.assertIn(mock.call('http://localhost:8000/api/v1/employee/3'), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)

    @mock.patch('requests.post', side_effect=mocked_requests_create)
    def test_create_employee_success(self, mock_request):

        serializer = EmployeeSerializer(instance=EmployeeFactory.create())
        payload = serializer.data
        del payload['id']

        response = self.employeeApi.create( payload )
        self.assertEqual( response.json_data["results"]["name"], payload["name"])
        self.assertEqual(response.json_data["results"]["email"], payload["email"])
        self.assertEqual(response.json_data["results"]["department"], payload["department"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.json_data["results"])
        self.assertIn(mock.call('http://localhost:8000/api/v1/employee', data=payload), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)

    @mock.patch('requests.post', side_effect=mocked_requests_create)
    def test_create_employee_fail(self, mock_request):

        response = self.employeeApi.create( None )

        self.assertEqual(response.json_data["detail"], "Internal server error.")
        self.assertEqual(response.status_code, 500)

        self.assertIn(mock.call('http://localhost:8000/api/v1/employee', data=None), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)



    @mock.patch('requests.put', side_effect=mocked_requests_update)
    def test_update_employee_success(self, mock_request):

        serializer = EmployeeSerializer(instance=EmployeeFactory.create())
        payload = serializer.data

        self.assertEqual(payload["name"], "aurelio")
        self.assertEqual(payload["email"], "aurelio@gmail.com")
        self.assertEqual(payload["department"], "TI")
        del payload['id']

        payload["name"] = "aurelio updated"
        payload["email"] = "aurelio1@gmail.com"
        payload["department"] = "TI2"
        response = self.employeeApi.update(1, payload)

        self.assertEqual(response.json_data["results"]["name"], payload["name"])
        self.assertEqual(response.json_data["results"]["email"], payload["email"])
        self.assertEqual(response.json_data["results"]["department"], payload["department"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.json_data["results"])
        self.assertIn(mock.call('http://localhost:8000/api/v1/employee/1', data=payload), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)


    @mock.patch('requests.put', side_effect=mocked_requests_update)
    def test_update_employee_fail(self, mock_request):

        response = self.employeeApi.update(2,  None )

        self.assertEqual(response.json_data["detail"], "Internal server error.")
        self.assertEqual(response.status_code, 500)

        self.assertIn(mock.call('http://localhost:8000/api/v1/employee/2', data=None), mock_request.call_args_list)
        self.assertEqual(len(mock_request.call_args_list), 1)