from employee.models.employee import Employee
import factory

class EmployeeFactory(factory.DjangoModelFactory):
    class Meta:
        model = Employee

    name = factory.Sequence(lambda n: 'aurelio')
    email = factory.LazyAttribute(lambda o: '%s@gmail.com' % o.name)
    department = factory.Sequence(lambda n: 'TI')