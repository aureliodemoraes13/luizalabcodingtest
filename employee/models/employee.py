from django.db import models

class Employee(models.Model):
    name = models.CharField(max_length=255, null=False)
    department = models.CharField(max_length=150, null=False)
    email = models.EmailField(max_length=255, null=False)

    def __str__(self):
        return str("name: {0}, email: {1}, department: {2}".format(self.name, self.email, self.department) )

    def __cmp__(self, other):
        return self.__dict__ == other.__dict__
