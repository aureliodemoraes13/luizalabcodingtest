import requests
from luizalabCodingTest import config


class EmployeeApi(object):

    def list(self, page):
        employees_list = requests.get( "{0}/?page={1}".format( config.API["url_employee"], page ) )
        employees_json = employees_list.json()
        return employees_json

    def delete(self, id):
        res = requests.delete("{0}/{1}".format(config.API["url_employee"],id))
        return res

    def create(self, employee_paylod):
        res = requests.post(config.API["url_employee"], data=employee_paylod)
        return res

    def update(self, id, employee_paylod):
        res = requests.put("{0}/{1}".format(config.API["url_employee"],id), data=employee_paylod)
        return res


